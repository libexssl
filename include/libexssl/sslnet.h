#ifndef __SSLWNET_H__
#define __SSLWNET_H__

#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <libex/str.h>
#include <libex/net.h>
#include <libex/wsnet.h>

#define FL_WSLOADEDHDR 0x00000002
#define FL_WSLOADEDLEN 0x00000004
#define FL_WSLOADEDMASK 0x00000008
#define FL_WSLOADED 0x00000010

#define SSL_CLIENT 0
#define SSL_SERVER 1

extern __thread int ssl_errno;
cstr_t *ssl_error ();

SSL_CTX *ssl_create (int kind);
int ssl_config (SSL_CTX *ctx, const char *cert_file, const char *key_file);

int ssl_accept (SSL_CTX *ctx, int fd, SSL **ssl);
int ssl_connect (SSL_CTX *ctx, int fd, SSL **ssl);
ssize_t ssl_recv (SSL *ssl, strbuf_t *buf);
ssize_t ssl_send (SSL *ssl, const void *buf, size_t size);

int sslws_handshake (SSL *ssl, strbuf_t *buf, strptr_t *url);
int sslws_recv (SSL *ssl, netbuf_t *nbuf, ws_t *result);

#define NF_SSLACCEPTED 0x80000000
typedef struct ssl_ev ssl_ev_t;
typedef struct ssl_srv ssl_srv_t;

struct ssl_ev {
    DECLARE_EV;
    SSL *ssl;
} __attribute__ ((aligned(64)));
int ssl_free_ev (int fd, net_srv_t *srv);

struct ssl_srv {
    DECLARE_NET_SRV
    SSL_CTX *ctx_cli;
    SSL_CTX *ctx_srv;
};
#define SSLSRV_SIZE sizeof(ssl_srv_t)
#define SSLEV_SIZE sizeof(ssl_ev_t)
net_srv_t *sslws_srv_init_ex (const char *svc, size_t srv_size, size_t ev_size, const char *cert_file, const char *key_file);
static inline net_srv_t *sslws_srv_init (const char *svc, const char *cert_file, const char *key_file) {
    return sslws_srv_init_ex(svc, sizeof(ssl_srv_t), sizeof(ssl_ev_t), cert_file, key_file);
};
void sslws_srv_done (net_srv_t *srv);

net_srv_t *sslws_jsonrpc_srv_init_ex (const char *svc, size_t srv_size, size_t ev_size, const char *cert_file, const char *key_file);
static inline net_srv_t *sslws_jsonrpc_srv_init (const char *svc, const char *cert_file, const char *key_file) {
    return sslws_jsonrpc_srv_init_ex(svc, sizeof(ssl_srv_t), sizeof(ssl_ev_t), cert_file, key_file);
}

#endif
