#ifndef __SSLW_H__
#define __SSLW_H__

#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <openssl/bn.h>
#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/sha.h>
#include <openssl/rand.h>
#include <libex/str.h>
#include <libex/file.h>

typedef struct ssl_key ssl_key_t;
typedef struct ssl_key_pair ssl_key_pair_t;

const char *ssl_key_msg (ssl_key_t *key);
const char *ssl_keys_msg (ssl_key_pair_t *keys);

int ssl_init ();
void ssl_done ();

int ssl_gen_keys (int nbits, ssl_key_pair_t *keys);
int ssl_gen_keys_fp (int nbits, FILE *fp_priv, FILE *fp_pub, const char *passwd, ssl_key_pair_t *keys);
int ssl_gen_keys_fl (int nbits, const char *priv, const char *pub, const char *passwd, ssl_key_pair_t *keys);
int ssl_gen_keys_buf (int nbits, strptr_t *priv, strptr_t *pub, const char *passwd, ssl_key_pair_t *keys);
int ssl_load_priv_fp (FILE *fp, const char *passwd, ssl_key_t *key);
int ssl_load_priv_fl (const char *fname, const char *passwd, ssl_key_t *key);
int ssl_load_priv_buf (const char *passwd, ssl_key_t *key, strptr_t *buf);
int ssl_load_pub_fp (FILE *fp, ssl_key_t *key);
int ssl_load_pub_fl (const char *fname, ssl_key_t *key);
int ssl_load_pub_buf (const char *buf, size_t buf_len, ssl_key_t *key);

int ssl_encrypt_pub (ssl_key_t *key, const char *buf, size_t buf_len, str_t **result);
int ssl_decrypt_priv (ssl_key_t *key, const char *buf, size_t buf_len, str_t **result);

int ssl_sign (const unsigned char *msg, size_t mlen, unsigned char **sig, size_t *slen, int dig, ssl_key_t *pkey);
int ssl_verify (const unsigned char *msg, size_t mlen, const unsigned char *sig, size_t slen, int dig, ssl_key_t *pkey);

ssl_key_t *ssl_key_init ();
ssl_key_pair_t *ssl_key_pair_init ();

void ssl_free_key (ssl_key_t *key);
void ssl_free_keys (ssl_key_pair_t *keys);

#endif // __SSLW_H__
