#include "sslnet.h"

static __thread str_t *errmsg = NULL;
__thread int ssl_errno = 0;

__attribute__ ((destructor))
static void sslw_free_err () {
    if (errmsg)
        free(errmsg);
}

static int adderr (const char *str, size_t len, void *userdata) {
    if (errmsg)
        strnadd(&errmsg, str, len);
    else
        errmsg = mkstr(str, len, 64);
    return 0;
}

cstr_t *ssl_error () {
    if (errmsg)
        return mkcstr(errmsg->ptr, errmsg->len);
    return NULL;
}

SSL_CTX *ssl_create (int kind) {
    const SSL_METHOD *m;
    switch (kind) {
        case SSL_CLIENT:
            m = SSLv23_client_method();
            break;
        case SSL_SERVER:
            m = SSLv23_server_method();
            break;
        default:
            m = SSLv23_method();
            break;
    }
    SSL_CTX *ctx = SSL_CTX_new(m);
    if (!ctx) {
        if (errmsg) {
            free(errmsg);
            errmsg = NULL;
        }
        ERR_print_errors_cb(adderr, NULL);
    }
    return ctx;
}

int ssl_config (SSL_CTX *ctx, const char *cert_file, const char *key_file) {
    int rc;
    SSL_CTX_set_ecdh_auto(ctx, 1);
    if ((rc = SSL_CTX_use_certificate_chain_file(ctx, cert_file)) > 0)
        rc = SSL_CTX_use_PrivateKey_file(ctx, key_file, SSL_FILETYPE_PEM);
    if (rc <= 0) {
        if (errmsg) {
            free(errmsg);
            errmsg = NULL;
        }
        ERR_print_errors_cb(adderr, NULL);
    }
    return rc > 0 ? 0 : -1;
}

int ssl_accept (SSL_CTX *ctx, int fd, SSL **ssl) {
    int rc;
    if (!*ssl) {
        *ssl = SSL_new(ctx);
        SSL_set_fd(*ssl, fd);
    } else
    if (SSL_is_init_finished(*ssl))
        return NET_WAIT;
    rc = SSL_accept(*ssl);
    if (1 == rc)
        return 1;
    switch (SSL_get_error(*ssl, rc)) {
        case SSL_ERROR_NONE:
            return 1;
        case SSL_ERROR_WANT_WRITE:
        case SSL_ERROR_WANT_READ:
            return 0;
        default:
            if (errmsg) {
                free(errmsg);
                errmsg = NULL;
            }
            ERR_print_errors_cb(adderr, NULL);
            SSL_shutdown(*ssl);
            SSL_free(*ssl);
            *ssl = NULL;
    }
    return -1;
}

int ssl_connect (SSL_CTX *ctx, int fd, SSL **ssl) {
    int rc;
    if (!*ssl) {
        *ssl = SSL_new(ctx);
        SSL_set_fd(*ssl, fd);
    }
    if (SSL_is_init_finished(*ssl))
        return 0;
    rc = SSL_connect(*ssl);
    if (1 == rc)
        return 1;
    switch (SSL_get_error(*ssl, rc)) {
        case SSL_ERROR_NONE:
            return 1;
        case SSL_ERROR_WANT_WRITE:
        case SSL_ERROR_WANT_READ:
            return 0;
        default:
            if (errmsg) {
                free(errmsg);
                errmsg = NULL;
            }
            ERR_print_errors_cb(adderr, NULL);
            SSL_shutdown(*ssl);
            SSL_free(*ssl);
            *ssl = NULL;
    }
    return -1;
}

ssize_t ssl_recv (SSL *ssl, strbuf_t *buf) {
    ssize_t readed = -1;
    int rc;
    ssl_errno = 0;
    if (-1 == strbufsize(buf, buf->len + buf->chunk_size, STR_KEEPLEN | STR_REDUCE))
        return -1;
    rc = SSL_read_ex(ssl, buf->ptr + buf->len, buf->chunk_size, (size_t*)&readed);
    if (rc > 0) {
        buf->len += readed;
        return readed;
    }
    switch ((ssl_errno = SSL_get_error(ssl, rc))) {
        case SSL_ERROR_WANT_READ:
        case SSL_ERROR_NONE:
            if (0 == rc)
                ssl_errno = 0;
            return 0;
        case SSL_ERROR_ZERO_RETURN:
        case SSL_ERROR_SYSCALL:
            SSL_shutdown(ssl);
            return -1;
        default:
            if (errmsg) {
                free(errmsg);
                errmsg = NULL;
            }
            ERR_print_errors_cb(adderr, NULL);
            break;
    }
    return -1;
}

ssize_t ssl_send (SSL *ssl, const void *buf, size_t size) {
    ssize_t sent = 0, wrote = 0;
    int rc;
    ssl_errno = 0;
    while (sent < size) {
        rc = SSL_write_ex(ssl, buf, size - sent, (size_t*)&wrote);
        if (rc > 0) {
            sent += wrote;
            buf += wrote;
            continue;
        }
        if (0 == rc)
            continue;
        if (errmsg) {
            free(errmsg);
            errmsg = NULL;
        }
        ssl_errno = SSL_get_error(ssl, rc);
        if (SSL_ERROR_WANT_WRITE == ssl_errno)
            continue;
        if (SSL_ERROR_ZERO_RETURN == ssl_errno)
            SSL_shutdown(ssl);
        ERR_print_errors_cb(adderr, NULL);
        sent = -1;
    }
    return sent;
}

int sslws_handshake (SSL *ssl, strbuf_t *buf, strptr_t *url) {
    int rc = WS_ERROR;
    ssize_t readed;
    while ((readed = ssl_recv(ssl, buf)) > 0);
    if (0 == readed && SSL_ERROR_WANT_READ == ssl_errno)
        return WS_WAIT;
    if (-1 == readed)
        return WS_ERROR;
    http_request_t req;
    ws_handshake_t wsh;
    memset(&req, 0, sizeof req);
    memset(&wsh, 0, sizeof wsh);
    switch ((rc = ws_handshake(&req, buf->ptr, buf->len, &wsh))) {
        case HTTP_LOADED:
            if (url) {
                url->ptr = strndup(req.url.ptr, req.url.len);
                url->len = req.url.len;
            }
            ws_make_response(buf, &wsh);
            if ((readed = ssl_send(ssl, buf->ptr, buf->len)) > 0) {
                rc = WS_OK;
                buf->len = 0;
                return rc;
            }
            rc = WS_ERROR;
            buf->len = 0;
            if (url) {
                if (url->ptr) {
                    free(url->ptr);
                    url->ptr = NULL;
                }
                url->len = 0;
            }
            break;
        case HTTP_ERROR:
            rc = WS_ERROR;
            break;
        default:
            rc = WS_WAIT;
            break;
    }
    return rc;
}

int sslws_recv (SSL *ssl, netbuf_t *nbuf, ws_t *result) {
    int rc = 0;
    ssize_t nbytes, ntotal = 0;
    if ((nbytes = ws_buflen((const uint8_t*)nbuf->buf.ptr, nbuf->buf.len)) > 0) {
        rc = ws_parse(nbuf->buf.ptr, nbytes, result);
        wsnet_save_tail(nbuf, nbytes);
        if (WS_OK == rc)
            return WS_OK;
        return WS_WAIT;
    }
    while ((nbytes = ssl_recv(ssl, &nbuf->buf)) > 0)
        ntotal += nbytes;
    if (0 == nbytes && SSL_ERROR_WANT_READ == ssl_errno)
        return WS_WAIT;
    if (-1 == nbytes)
        return WS_ERROR;
    if (-1 == (nbytes = ws_buflen((const uint8_t*)nbuf->buf.ptr, nbuf->buf.len)))
        return WS_WAIT;
    rc = ws_parse(nbuf->buf.ptr, nbytes, result);
    wsnet_save_tail(nbuf, nbytes);
    if (WS_OK == rc)
        return WS_OK;
    return WS_WAIT;
}

// WS SRV

static int sslws_ev_open (int fd, ssl_srv_t *srv, uint32_t flags, http_url_t *h_url) {
    int rc = NET_ERROR;
    ssize_t rd;
    ssl_ev_t *ev = srv->get_ev((net_srv_t*)srv, fd);
    str_t *req;
    strbuf_t buf;
    if (!(flags & NF_CLIENT))
        return NET_OK;
    switch (ssl_connect(srv->ctx_cli, fd, &ev->ssl)) {
        case -1:
            return NET_ERROR;
        case 0:
            return NET_ERROR;
        case 1:
            ev->nf_flags |= NF_SSLACCEPTED;
            break;
    }
    req = create_ws_request(h_url);
    strbufalloc(&buf, SRV_BUF_SIZE, SRV_BUF_SIZE);
    if (req->len == ssl_send(ev->ssl, req->ptr, req->len)) {
        while ((rd = ssl_recv(ev->ssl, &buf)) > 0) {
            if (buf.len > 4 && 0 == cmpstr(buf.ptr+buf.len-4, 4, CONST_STR_LEN("\r\n\r\n"))) { // FIXME : parse response, save tail
                net_ev_t *ev = srv->get_ev((net_srv_t*)srv, fd);
                rc = NET_WAIT;
                ev->nf_flags |= NF_WSCONNECTED;
                break;
            }
        }
    }
    free(buf.ptr);
    free(req);
    return rc;
}

static int sslws_ev_recv (int fd, int flags, ssl_srv_t *srv) {
    ssl_ev_t *ev = srv->get_ev((net_srv_t*)srv, fd);
    int rc;
    if (!(ev->nf_flags & NF_SSLACCEPTED)) {
        switch (ssl_accept(srv->ctx_srv, fd, &ev->ssl)) {
            case -1:
                return NET_ERROR;
            case 0:
                return NET_WAIT;
            case 1:
                ev->nf_flags |= NF_SSLACCEPTED;
                break;
        }
    }
    if (!(ev->nf_flags & NF_WSCONNECTED)) {
        strptr_t url = CONST_STR_INIT_NULL;
        switch (sslws_handshake(ev->ssl, &ev->rd_buf.buf, &url)) {
            case WS_WAIT:
                return NET_WAIT;
            case WS_ERROR:
                return NET_ERROR;
        }
        ticket_lock(&ev->locker);
        ev->nf_flags |= NF_WSCONNECTED;
        if (ev->rd_buf.buf.ptr) {
            free(ev->rd_buf.buf.ptr);
            ev->rd_buf.buf.ptr = NULL;
        }
        if (url.ptr) {
            if (srv->on_check_url) {
                int ver;
                if (NET_ERROR == (ver = srv->on_check_url(fd, (net_srv_t*)srv, &url))) {
                    ticket_unlock(&ev->locker);
                    free(url.ptr);
                    return NET_ERROR;
                }
                ev->ver = ver;
            }
            free(url.ptr);
        }
        ticket_unlock(&ev->locker);
        return NET_OK;
    }
    if (NF_READ == flags)
        rc = sslws_recv(ev->ssl, &ev->rd_buf, &ev->ws);
    else
        rc = wsnet_parse(&ev->rd_buf, &ev->ws);
    switch (rc) {
        case WS_WAIT:
            return NET_WAIT;
        case WS_ERROR:
            return NET_ERROR;
    }
    return NET_OK;
}

static int ssl_ev_send (int fd, ssl_srv_t *srv) {
    ssize_t rc;
    cstr_t *buf;
    ssl_ev_t *ev = srv->get_ev((net_srv_t*)srv, fd);
    if (!ev->ssl)
        return NET_OK;
    ticket_lock(&ev->locker);
    buf = mkcstr(ev->wr_buf.ptr + ev->wrote, ev->wr_buf.len - ev->wrote);
    ticket_unlock(&ev->locker);
    if ((rc = ssl_send(ev->ssl, buf->ptr, buf->len)) > 0)
        ev->wrote += rc;
    free(buf);
    return rc;
}

static int ssl_close_ev (int fd, ssl_srv_t *srv) {
    ssl_ev_t *ev = srv->get_ev((net_srv_t*)srv, fd);
    if (ev->ssl) {
        SSL_shutdown(ev->ssl);
        SSL_free(ev->ssl);
        ev->ssl = NULL;
    }
    return NET_OK;
}

int ssl_free_ev (int fd, net_srv_t *srv) {
    ssl_ev_t *ev = srv->get_ev(srv, fd);
    if (ev->ssl) {
        SSL_shutdown(ev->ssl);
        SSL_free(ev->ssl);
    }
    return ev_free_ev(fd, srv);
}

static void *get_ev (net_srv_t *srv, int fd) {
    return &((ssl_ev_t*)srv->fd_info)[fd];
}

net_srv_t *sslws_srv_init_ex (const char *svc, size_t srv_size, size_t ev_size, const char *cert_file, const char *key_file) {
    net_srv_t *srv = net_srv_init(svc, srv_size, ev_size);
    SSL_CTX *ctx_cli = NULL, *ctx_srv = NULL;
    if (!srv) return NULL;
    if (!(ctx_cli = ssl_create(SSL_CLIENT)) ||
        !(ctx_srv = ssl_create(SSL_SERVER)) ||
        -1 == ssl_config(ctx_cli, cert_file, key_file) ||
        -1 == ssl_config(ctx_srv, cert_file, key_file))
            goto err;
    ((ssl_srv_t*)srv)->ctx_cli = ctx_cli;
    ((ssl_srv_t*)srv)->ctx_srv = ctx_srv;
    srv->on_open = (net_ev_open_h)sslws_ev_open;
    srv->on_recv = (net_ev_recv_h)sslws_ev_recv;
    srv->on_send = (net_ev_h)ssl_ev_send;
    srv->on_recvd = ws_ev_recvd;
    srv->ev_bufsize = sizeof(ev_buf_t);
    srv->on_evbuf_set = ws_evbuf_set;
    srv->on_evbuf_free = ws_rpc_evbuf_free;
    srv->on_event = ws_rpc_srv_event;
    srv->on_close_ev = (net_ev_h)ssl_close_ev;
    srv->on_free_ev = ssl_free_ev;
    srv->msg_info = ws_create_msg_info();
    srv->get_ev = get_ev;
    return srv;
err:
    if (ctx_srv) SSL_CTX_free(ctx_srv);
    if (ctx_cli) SSL_CTX_free(ctx_cli);
    net_srv_done(srv);
    return NULL;
}

void sslws_srv_done (net_srv_t *srv) {
    SSL_CTX *ctx;
    if ((ctx = ((ssl_srv_t*)srv)->ctx_srv))
        SSL_CTX_free(ctx);
    if ((ctx = ((ssl_srv_t*)srv)->ctx_cli))
        SSL_CTX_free(ctx);
    rbtree_free(srv->msg_info);
    ws_srv_done(srv);
}

// WS JSONRPC SRV

net_srv_t *sslws_jsonrpc_srv_init_ex (const char *svc, size_t srv_size, size_t ev_size, const char *cert_file, const char *key_file) {
    net_srv_t *srv = sslws_srv_init_ex(svc, srv_size, ev_size, cert_file, key_file);
    if (srv) {
        srv->on_evbuf_free = ws_jsonrpc_evbuf_free;
        srv->on_event = ws_jsonrpc_srv_event;
        srv->on_check_url = check_api_ver;
    }
    return srv;
}
