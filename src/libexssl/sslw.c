#include "ssl.h"

struct ssl_key {
    str_t *msg;
    RSA *rsa;
    EVP_PKEY *key;
};

struct ssl_key_pair {
    str_t *msg;
    RSA *rsa;
    EVP_PKEY *pub;
    EVP_PKEY *priv;
};

const char *ssl_key_msg (ssl_key_t *key) {
    return key && key->msg ? (const char*)key->msg->ptr : NULL;
}

const char *ssl_keys_msg (ssl_key_pair_t *keys) {
    return keys && keys->msg ? (const char*)keys->msg->ptr : NULL;
}

int ssl_init () {
    if (!SSL_library_init()) return -1;
    SSL_load_error_strings();
    OpenSSL_add_all_algorithms();
    RAND_load_file("/dev/urandom", 1024);
    OpenSSL_add_ssl_algorithms();
    return 0;
}

void ssl_done () {
    CRYPTO_cleanup_all_ex_data();
    ERR_free_strings();
#if !(OPENSSL_API_COMPAT < 0x10100000L)
    ERR_remove_thread_state(0);
#endif
    EVP_cleanup();
}

static str_t *create_error_msg () {
    str_t *s = stralloc(64, 64);
    ERR_print_errors_cb(({
        int fn (const char *str, size_t len, void *u) {
            str_t **err_str = (str_t**)u;
            strnadd(err_str, str, len);
            return (int)(*err_str)->len;
        } fn;
    }), &s);
    return s;
}

static int create_rsa_key (RSA *rsa, int nbits, ssl_key_t *ssl_key) {
    EVP_PKEY *key = NULL;
    key = EVP_PKEY_new();
    if (key && EVP_PKEY_set1_RSA(key, rsa)) {
        ssl_key->key = key;
        return 0;
    }
    ssl_key->msg = create_error_msg();
    if (key) EVP_PKEY_free(key);
    return -1;
}

#if OPENSSL_API_COMPAT < 0x10100000L
static RSA *gen_keys (int bits, unsigned long e_value) {
    int i;
    BN_GENCB *cb = BN_GENCB_new();
    RSA *rsa = RSA_new();
    BIGNUM *e = BN_new();
    if (cb == NULL || rsa == NULL || e == NULL)
        goto err;
    for (i = 0; i < (int)sizeof(unsigned long) * 8; i++) {
        if (e_value & (1UL << i))
            if (BN_set_bit(e, i) == 0)
                goto err;
    }
    BN_GENCB_set_old(cb, NULL, NULL);
    if (bits < 1024)
        bits = 1024;
    if (RSA_generate_key_ex(rsa, bits, e, cb)) {
        BN_free(e);
        BN_GENCB_free(cb);
        return rsa;
    }
 err:
    BN_free(e);
    RSA_free(rsa);
    OPENSSL_free(cb);
    return 0;
}
#endif

int ssl_gen_keys (int nbits, ssl_key_pair_t *keys) {
    ssl_key_t pub = { .msg = NULL, .key = NULL, .rsa = NULL },
             priv = { .msg = NULL, .key = NULL, .rsa = NULL };
#if OPENSSL_API_COMPAT < 0x10100000L
    RSA *rsa = gen_keys(nbits, RSA_F4);
#else
    RSA *rsa = RSA_generate_key(nbits, RSA_F4, NULL, NULL);
#endif
    if (!rsa) {
        keys->msg = create_error_msg();
        return -1;
    }
    if (-1 == create_rsa_key(rsa, nbits, &priv)) {
        RSA_free(rsa);
        keys->msg = priv.msg;
        return -1;
    }
    if (-1 == create_rsa_key(rsa, nbits, &pub)) {
        RSA_free(rsa);
        EVP_PKEY_free(priv.key);
        priv.key = NULL;
        keys->msg = pub.msg;
        return -1;
    }
    if (0 >= RSA_check_key(rsa)) {
        RSA_free(rsa);
        EVP_PKEY_free(priv.key);
        EVP_PKEY_free(pub.key);
        keys->msg = create_error_msg();
        return -1;
    }
    keys->rsa = rsa;
    keys->priv = priv.key;
    keys->pub = pub.key;
    return 0;
}

int ssl_gen_keys_fp (int nbits, FILE *fp_priv, FILE *fp_pub, const char *passwd, ssl_key_pair_t *keys) {
    int ret;
    if (-1 == (ret = ssl_gen_keys(nbits, keys))) return -1;
    if (!PEM_write_PUBKEY(fp_pub, keys->pub)) {
        keys->msg = create_error_msg();
        return -1;
    }
    strptr_t pass = { .len = 0, .ptr = "" };
    if (passwd) {
        pass.ptr = (char*)passwd;
        pass.len = strlen(passwd);
    }
    const EVP_CIPHER *cipher = EVP_aes_256_cbc();
    if (!PEM_write_PrivateKey(fp_priv, keys->priv, cipher, (unsigned char*)pass.ptr, (int)pass.len, NULL, NULL)) {
        keys->msg = create_error_msg();
        return -1;
    }
    return 0;
}

static str_t *create_errno_msg (const char *fname) {
    char *errno_msg = strerror(errno);
    str_t *msg = mkstr(errno_msg, strlen(errno_msg), 64);
    if (fname) {
        strnadd(&msg, CONST_STR_LEN("; \""));
        strnadd(&msg, fname, strlen(fname));
        strnadd(&msg, CONST_STR_LEN("\""));
    }
    return msg;
}

int ssl_gen_keys_fl (int nbits, const char *priv, const char *pub, const char *passwd, ssl_key_pair_t *keys) {
    FILE *fp_priv = fopen(priv, "wt");
    if (!fp_priv) {
        keys->msg = create_errno_msg(priv);
        return -1;
    }
    FILE *fp_pub = fopen(pub, "wt");
    if (!fp_pub) {
        fclose(fp_priv);
        keys->msg = create_errno_msg(pub);
        return -1;
    }
    int ret = ssl_gen_keys_fp(nbits, fp_priv, fp_pub, passwd, keys);
    fclose(fp_pub);
    fclose(fp_priv);
    return ret;
}

int ssl_gen_keys_buf (int nbits, strptr_t *priv, strptr_t *pub, const char *passwd, ssl_key_pair_t *keys) {
    priv->len = nbits;
    priv->ptr = malloc(priv->len);
    FILE *fp_priv = open_memstream(&priv->ptr, &priv->len);
    if (!fp_priv) {
        free(priv->ptr);
        priv->ptr = NULL;
        priv->len = 0;
        keys->msg = create_errno_msg(NULL);
        return -1;
    }
    pub->len = nbits / 2;
    pub->ptr = malloc(pub->len);
    FILE *fp_pub = open_memstream(&pub->ptr, &pub->len);
    if (!fp_pub) {
        fclose(fp_priv);
        free(priv->ptr);
        priv->ptr = NULL;
        priv->len = 0;
        free(pub->ptr);
        pub->ptr = NULL;
        pub->len = 0;
        keys->msg = create_errno_msg(NULL);
        return -1;
    }
    int ret = ssl_gen_keys_fp(nbits, fp_priv, fp_pub, passwd, keys);
    if (0 == ret && (1 != fwrite("\0", 1, 1, fp_priv) || 1 != fwrite("\0", 1, 1, fp_pub))) {
        keys->msg = create_errno_msg(NULL);
        ret = -1;
    }
    fclose(fp_pub);
    fclose(fp_priv);
    return ret;
}

static int on_password_cb (char *buf, int size, int rwflags, void *u) {
    size_t upass = strlen((char*)u);
    if (upass > (size_t)size)
        upass = (size_t)size;
    memcpy(buf, u, upass);
    return (int)upass;
}

int ssl_load_priv_fp (FILE *fp, const char *passwd, ssl_key_t *key) {
    int ret = 0;
    pem_password_cb *fn = passwd ? on_password_cb : NULL;
    if (!PEM_read_PrivateKey(fp, &key->key, fn, (void*)passwd)) {
        key->msg = create_error_msg();
        ret = -1;
    } else {
        if (!(key->rsa = EVP_PKEY_get1_RSA(key->key))) {
            RSA_free(key->rsa);
            key->rsa = NULL;
            EVP_PKEY_free(key->key);
            key->key = NULL;
            key->msg = create_error_msg();
            ret = -1;
        }
    }
    return ret;
}

int ssl_load_priv_fl (const char *fname, const char *passwd, ssl_key_t *key) {
    int ret = -1;
    FILE *fp = fopen(fname, "r");
    if (fp) {
        ret = ssl_load_priv_fp(fp, passwd, key);
        fclose(fp);
    } else
        key->msg = create_errno_msg(fname);
    return ret;
}

int ssl_load_priv_buf (const char *passwd, ssl_key_t *key, strptr_t *buf) {
    int ret = -1;
    FILE *fp = fmemopen(buf->ptr, buf->len, "r");
    if (fp) {
        ret = ssl_load_priv_fp(fp, passwd, key);
        fclose(fp);
    } else
        key->msg = create_errno_msg(NULL);
    return ret;
}

int ssl_load_pub_fp (FILE *fp, ssl_key_t *key) {
    int ret = 0;
    if (!PEM_read_PUBKEY(fp, &key->key, NULL ,NULL)) {
        key->msg = create_error_msg();
        ret = -1;
    } else {
        if (!(key->rsa = EVP_PKEY_get1_RSA(key->key))) {
            RSA_free(key->rsa);
            EVP_PKEY_free(key->key);
            key->key = NULL;
            key->msg = create_error_msg();
            ret = -1;
        }
    }
    return ret;
}

int ssl_load_pub_fl (const char *fname, ssl_key_t *key) {
    int ret = -1;
    FILE *fp = fopen(fname, "r");
    if (fp) {
        ret = ssl_load_pub_fp(fp, key);
        fclose(fp);
    } else
        key->msg = create_errno_msg(fname);
    return ret;
}

int ssl_load_pub_buf (const char *buf, size_t buf_len, ssl_key_t *key) {
    int ret = -1;
    FILE *fp = fmemopen((void*)buf, buf_len, "r");
    if (fp) {
        ret = ssl_load_pub_fp(fp, key);
        fclose(fp);
    } else
        key->msg = create_errno_msg(NULL);
    return ret;
}

int ssl_encrypt_pub (ssl_key_t *key, const char *buf, size_t buf_len, str_t **result) {
    int encrypt_len = RSA_size(key->rsa);
    str_t *ret = stralloc(encrypt_len, 64);
    *result = NULL;
    if (-1 == (encrypt_len = RSA_public_encrypt(buf_len, (unsigned char*)buf, (unsigned char*)ret->ptr, key->rsa, RSA_PKCS1_OAEP_PADDING))) {
        key->msg = create_error_msg();
        free(ret);
        return -1;
    }
    ret->len = encrypt_len;
    *result = ret;
    return 0;
}

int ssl_decrypt_priv (ssl_key_t *key, const char *buf, size_t buf_len, str_t **result) {
    int encrypt_len = RSA_size(key->rsa);
    str_t *res = stralloc(encrypt_len, 64);
    *result = NULL;
    memset(res->ptr, 0, res->bufsize);
    if (-1 == (encrypt_len = RSA_private_decrypt(buf_len, (unsigned char*)buf, (unsigned char*)res->ptr, key->rsa, RSA_PKCS1_OAEP_PADDING))) {
        key->msg = create_error_msg();
        free(res);
        return -1;
    }
    res->len = encrypt_len;
    *result = res;
    return 0;
}

int ssl_sign (const unsigned char *msg, size_t mlen, unsigned char **sig, size_t *slen, int dig, ssl_key_t *pkey) {
    int rc = -1;
    EVP_MD_CTX *ctx;
    const EVP_MD *md;
    if (*sig) {
        OPENSSL_free(*sig);
        *sig = NULL;
    }
    *slen = 0;
    if (!(ctx = EVP_MD_CTX_create()))
        return -1;
    if (!dig)
        dig = NID_sha1WithRSAEncryption;
    if (!(md = EVP_get_digestbynid(dig)) ||
        0 == EVP_DigestInit_ex(ctx, md, NULL) ||
        0 == EVP_DigestSignInit(ctx, NULL, md, NULL, pkey->key) ||
        0 == EVP_DigestSignUpdate(ctx, msg, mlen) ||
        0 == EVP_DigestSignFinal(ctx, NULL, slen) ||
        !(*sig = OPENSSL_malloc(*slen)) ||
        0 == EVP_DigestSignFinal(ctx, *sig, slen))
        goto done;
    rc = 0;
done:
    if (-1 == rc) {
        if (*sig) {
            OPENSSL_free(*sig);
            *sig = NULL;
        }
        *slen = 0;
    }
    EVP_MD_CTX_destroy(ctx);
    return rc;
}

int ssl_verify (const unsigned char *msg, size_t mlen, const unsigned char *sig, size_t slen, int dig, ssl_key_t *pkey) {
    int rc = -1;
    EVP_MD_CTX *ctx;
    const EVP_MD *md;
    if (!(ctx = EVP_MD_CTX_create()))
        return -1;
    if (!dig)
        dig = NID_sha1WithRSAEncryption;
    if (!(md = EVP_get_digestbynid(dig)) ||
        0 == EVP_DigestInit_ex(ctx, md, NULL) ||
        0 == EVP_DigestVerifyInit(ctx, NULL, md, NULL, pkey->key) ||
        0 == EVP_DigestVerifyUpdate(ctx, msg, mlen))
        goto done;
    ERR_clear_error();
    if (0 == EVP_DigestVerifyFinal(ctx, sig, slen))
        goto done;
    rc = 0;
done:
    EVP_MD_CTX_destroy(ctx);
    return rc;
}

ssl_key_t *ssl_key_init () {
    ssl_key_t *k = malloc(sizeof(ssl_key_t));
    k->rsa = NULL;
    k->key = NULL;
    k->msg = NULL;
    return k;
}

ssl_key_pair_t *ssl_key_pair_init () {
    ssl_key_pair_t *k = malloc(sizeof(ssl_key_pair_t));
    k->rsa = NULL;
    k->priv = NULL;
    k->pub = NULL;
    k->msg = NULL;
    return k;
}

void ssl_free_key (ssl_key_t *key) {
    if (key->key) EVP_PKEY_free(key->key);
    if (key->rsa) RSA_free(key->rsa);
    free(key);
}

void ssl_free_keys (ssl_key_pair_t *keys) {
    if (keys->rsa) RSA_free(keys->rsa);
    if (keys->priv) EVP_PKEY_free(keys->priv);
    if (keys->pub) EVP_PKEY_free(keys->pub);
    if (keys->msg) free(keys->msg);
    free(keys);
}
