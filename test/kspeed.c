#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>
#include <crpt.h>

int main () {
    int count = 0;
    char buf [256];
    if (-1 == cr_init()) return 1;
    cr_key_t *priv = cr_key_init(), *pub = cr_key_init();
    if (0 == cr_load_priv_fl("./priv.pem", "masterkey", priv) && 0 == cr_load_pub_fl("./pub.pem", pub)) {
        str_t *src = mkstr(CONST_STR_LEN("Good luck"), 8), *crp = NULL, *dcr = NULL;
        time_t t0 = time(0), t1 = t0 + 60;
        const struct tm *tm = localtime(&t0);
        strftime(buf, sizeof(buf), "%a, %d %b %Y %H:%M:%S GMT ", tm);
        printf("%s\n", buf);
        while (time(0) < t1) {
            if (0 == cr_encrypt_pub(pub, src->ptr, src->len, &crp)) {
                if (0 == cr_decrypt_priv(priv, crp->ptr, crp->len, &dcr)) {
                    free(dcr);
                    dcr = NULL;
                    ++count;
                }
                free(crp);
                crp = NULL;
            }
        }
        printf("%d\n", count);
        tm = localtime(&t1);
        strftime(buf, sizeof(buf), "%a, %d %b %Y %H:%M:%S GMT ", tm);
        printf("%s\n", buf);
    }
    cr_free_key(priv);
    cr_free_key(pub);
    cr_done();
}
