#include <string.h>
#include "ssl.h"

int main (int argc, const char *argv[]) {
    if (argc < 3) return 1;
    const unsigned char *txt = (const unsigned char*)argv[1];
    const char *pkn = argv[2];
    unsigned char *sig = NULL;
    size_t slen = 0,
           len = strlen(argv[1]);
    if (-1 == ssl_init())
        return 1;
    ssl_key_t *key = ssl_key_init();
    if (-1 == ssl_load_pub_fl(pkn, key))
        goto done;
    if (0 == ssl_sign(txt, len, &sig, &slen, 0, key)) {
        int rc = ssl_verify(txt, len, sig, slen, 0, key);
        printf("Signatute: ");
        for (size_t i = 0; i < slen; ++i)
            printf("%02X", sig[i]);
        printf("\nResult: %d\n", rc);
    }
done:
    if (sig)
        OPENSSL_free(sig);
    ssl_free_key(key);
    ssl_done();
    return 0;
}
