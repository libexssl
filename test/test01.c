#include <string.h>
#include <stdio.h>
#include <libex/str.h>
#include "ssl.h"

#define PRIV_KEY "-----BEGIN RSA PRIVATE KEY-----\n" \
    "MIICXQIBAAKBgQDagE7sF2hON701d2KtUUxZOQBFW9PVzSR7rRdBQ3FJ71UhxXyL\n" \
    "0kmSmFrv8OOqc4jH+1C4RULOjfcLG11oGINVi7Wsh4AH+21+RIarkRYvPpivQ7/b\n" \
    "jlo7TdGFn2eFJKSWmjDNy9IE65qVG7qTYUhmHbTQbgs79/Gubnxw9fgZrwIDAQAB\n" \
    "AoGBALyg4gE9H+XOhsBehUh8k72+0LYP1SuQwrsmLZpA389lFwhlleSbMLqmXnMf\n" \
    "cVuYC/AgzImX+VmaAziKcjPIXa9m2mcpmGS7tpn0URIszGga0oWKtdetrdPkS89b\n" \
    "GiMnkr6GLgyZVgPcQ7qtZOuH8lQXR0MaY+8efESetCaYdCGJAkEA8EI9+4SitIDM\n" \
    "v3WSnYcKoh6vwz/3ABcFpPstxzprL+lfEh1vPaKXH5qapWsoftgl92c3X79Hwapg\n" \
    "a6zZx3ZkowJBAOjRIs+DIkFP83uYo+5ysQPUUOpuwEYA7FIl2aAWxlthrNHEwsmF\n" \
    "U7JPVtcNa0UNgWv3J3wZmqe33QSDZV6u+4UCQQDSQZZOq5dcEa+dCf5h/1EN6X71\n" \
    "ht/Y40zcQbihNn2hM0Ew8DNupABO9xTJGMsannwzU8/A6fuY/0pcUKtRhEmzAkBL\n" \
    "jbMSTKgNMfj+Hybz9txjNb+clJxpG1uVvRrGzR1KAoSm/oeIBnCG6SBxYbVyD1P0\n" \
    "yZxStaxuyUg8creu5nJRAkBk5NW+bsO6HENwg/xiWSNXnR+3GJQoLyN9XM4pC5OA\n" \
    "LUQcr02lKiLL/xofdeFzBEVHb1EdvNqYdLX2665DVXmc\n" \
    "-----END RSA PRIVATE KEY-----\n"
#define PUB_KEY "-----BEGIN PUBLIC KEY-----\n" \
    "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDagE7sF2hON701d2KtUUxZOQBF\n" \
    "W9PVzSR7rRdBQ3FJ71UhxXyL0kmSmFrv8OOqc4jH+1C4RULOjfcLG11oGINVi7Ws\n" \
    "h4AH+21+RIarkRYvPpivQ7/bjlo7TdGFn2eFJKSWmjDNy9IE65qVG7qTYUhmHbTQ\n" \
    "bgs79/Gubnxw9fgZrwIDAQAB\n" \
    "-----END PUBLIC KEY-----\n"

int main (int argc, const char *argv[]) {
    if (argc != 2) return 1;
    if (-1 == ssl_init()) return 1;
    const char *src_str = argv[1];
    size_t src_len = strlen(src_str);
    if (src_len > 127) return 1;
    printf("source string: %s\n", src_str);
    FILE *f_src = fmemopen(CONST_STR_LEN(PUB_KEY), "r");
    if (f_src) {
        ssl_key_t *pub_key = ssl_key_init();
        if (0 == ssl_load_pub_fp(f_src, pub_key)) {
            str_t *crp_str = NULL;
            if (0 == ssl_encrypt_pub(pub_key, src_str, src_len, &crp_str)) {
                save_file("./_1", crp_str->ptr, crp_str->len);
                str_t *b64_str = str_base64_encode(crp_str->ptr, crp_str->len, 8);
                STR_ADD_NULL(b64_str);
                printf("encoded string: %s\n", b64_str->ptr);
                FILE *f_dst = fmemopen(CONST_STR_LEN(PRIV_KEY), "r");
                if (f_dst) {
                    ssl_key_t *priv_key = ssl_key_init();
                    if (0 == ssl_load_priv_fp(f_dst, NULL, priv_key)) {
                        str_t *ub64_str = str_base64_decode(b64_str->ptr, b64_str->len, 8), *dssl_str = NULL;
                        if (0 != cmpstr(crp_str->ptr, crp_str->len, ub64_str->ptr, ub64_str->len))
                            printf("NO\n");
                        if (0 == ssl_decrypt_priv(priv_key, ub64_str->ptr, ub64_str->len, &dssl_str)) {
                            printf("decoded string: %s\n", dssl_str->ptr);
                        }
                        free(dssl_str);
                        free(ub64_str);
                    }
                    ssl_free_key(priv_key);
                    fclose(f_dst);
                }
                free(b64_str);
                free(crp_str);
            }
        }
        ssl_free_key(pub_key);
        fclose(f_src);
    }
    ssl_done();
    return 0;
}
