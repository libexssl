#include <libex/file.h>
#include "ssl.h"

int main (int argc, const char *argv[]) {
    ssl_key_t *sk, *pk;
    str_t *str;
    unsigned char *sig = NULL;
    size_t siglen = 0;
    if (2 != argc) return 1;
    ssl_init();
    sk = ssl_key_init();
    pk = ssl_key_init();
    if (0 == ssl_load_priv_fl("./private.pem", "password", sk) && 0 == ssl_load_pub_fl("./public.pem", pk)) {
        str = load_all_file(argv[1], 32, 4096);
        if (0 == ssl_sign((const unsigned char*)str->ptr, str->len, &sig, &siglen, 0, sk)) {
            if (0 == ssl_verify((const unsigned char*)str->ptr, str->len, sig, siglen, 0, pk))
                printf("Ok");
            else
                printf("Fail");
        }
        free(str);
    }
    if (sig)
        OPENSSL_free(sig);
    ssl_free_key(pk);
    ssl_free_key(sk);
    ssl_done();
}
