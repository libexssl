#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include "ssl.h"

int main (int argc, const char *argv[]) {
    if (argc < 3) return 1;
    const char *in_fname = argv[1],
               *out_fname = argv[2];
    if (-1 == ssl_init()) return 1;
    str_t *src = load_all_file(in_fname, 512, 2048);
    if (src) {
        ssl_key_t *key = ssl_key_init();
        if (0 == ssl_load_pub_fl("./key.pub", key)) {
            str_t *dst = NULL;
            if (0 == ssl_encrypt_pub(key, src->ptr, src->len, &dst)) {
                FILE *fp = fopen(out_fname, "wt");
                if (fp) {
                    fwrite(dst->ptr, dst->len, 1, fp);
                    fclose(fp);
                }
                free(dst);
            }
        }
        ssl_free_key(key);
        free(src);
    }
    ssl_done();
    return 0;
}
