#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include "ssl.h"

int main () {
    if (-1 == ssl_init()) return 1;
    ssl_key_pair_t *keys = ssl_key_pair_init();
    strptr_t priv_buf = { .ptr = NULL, .len = 0 }, pub_buf = { .ptr = NULL, .len = 0 };
    if (-1 == ssl_gen_keys_buf(256, &priv_buf, &pub_buf, "Gp5GwIeGkzvM0t9Y35VDKX3WnxJXuYOB", keys)) {
        // TODO
    }
    ssl_free_keys(keys);
    ssl_done();
    return 0;
}
