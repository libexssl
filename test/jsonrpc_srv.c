#include "sslnet.h"
#include "linenoise.h"

net_srv_t *srv;
int is_active = 1;
jsonrpc_method_array_t *methods;
ev_fd_t fd_conn = 0;
pthread_rwlock_t locker;

static void sigexit (int sig) {
    if (SIGTERM == sig || SIGINT == sig) {
        is_active = 0;
        sslws_srv_done(srv);
        free(methods);
    }
}

static void _connect () {
    if (!is_active)
        return;
    pthread_rwlock_wrlock(&locker);
    while (0 == fd_conn) {
        fd_conn = net_ev_connect(srv, CONST_STR_LEN("ws://127.0.0.1:7878/api/v2"));
        sleep(8);
    }
    pthread_rwlock_unlock(&locker);
}

static void setsig () {
    struct sigaction sa;
    sa.sa_handler = sigexit;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
}

static int on_handle (ev_buf_t *ev) {
    int rc = NET_ERROR;
    ws_t *ws = &ev->data.ws;
    if (!WS_ISFIN(ws) || WS_TEXT != WS_OPCODE(ws))
        return NET_OK;
    if (!jsonrpc_is_response(&ev->data.json.jsonrpc)) {
        if ((rc = jsonrpc_execute_parsed(&ev->buf, WS_EV_OFFSET(ev), methods, ev, &ev->data.json.jsonrpc)) < 0)
            rc = ws_ev_send(srv, ev->fd, 0, ev->buf.ptr, ev->buf.len, WS_FIN, WS_TEXT);
        else
            rc = NET_OK;
    } else {
        size_t off = WS_EV_OFFSET(ev);
        char *s = strndup(ev->buf.ptr + off, ev->buf.len - off);
        printf("%s\n", s);
        free(s);
        rc = NET_OK;
    }
    if (0 == rc)
        return NET_OK;
    return NET_ERROR;
}

static void *on_start (void *param) {
    ws_srv_start(srv);
    return NULL;
}

static int cb_login (strbuf_t *buf, void *userdata) {
    cstr_t *str = userdata;
    json_begin_object(buf);
    json_add_str(buf, CONST_STR_LEN("login"), str->ptr, str->len, JSON_END);
    json_end_object(buf);
    return 0;
}

static int login (strbuf_t *buf, json_item_t **params, size_t params_len, intptr_t id, int id_len, void *userdata) {
    cstr_t *s_login = mkcstr(params[0]->data.s.ptr, params[0]->data.s.len);
    ev_buf_t *ev = userdata;
    ws_jsonrpc_response_send(srv, ev->fd, buf, id, id_len, cb_login, s_login);
    free(s_login);
    return 0;
}

static int cb_ok (strbuf_t *buf, void *dummy) {
    json_add_true(buf, CONST_STR_NULL, JSON_END);
    return 0;
}

static int getprops (strbuf_t *buf, json_item_t **params, size_t params_len, intptr_t id, int id_len, void *userdata) {
    ev_buf_t *ev = userdata;
    ws_jsonrpc_response_send(srv, ev->fd, buf, id, id_len, cb_ok, NULL);
    return 0;
}

static int notify (strbuf_t *buf, json_item_t **params, size_t params_len, intptr_t id, int id_len, void *userdata) {
    char *s = strndup(params[0]->data.s.ptr, params[0]->data.s.len);
    printf("NOTIFY: %s\n", s);
    free(s);
    return 0;
}

static int on_login (strbuf_t *buf, void *dummy) {
    char *s;
    s = linenoise("login: ", NULL);
    json_add_str(buf, CONST_STR_NULL, s, strlen(s), JSON_NEXT);
    s = linenoise("pass: ", NULL);
    json_add_str(buf, CONST_STR_NULL, s, strlen(s), JSON_END);
    return 0;
}

static int on_notify (strbuf_t *buf, void *dummy) {
    char *s = linenoise("notify: ", NULL);
    json_add_str(buf, CONST_STR_NULL, s, strlen(s), JSON_END);
    return 0;
}

json_t *_send (char *method, size_t method_len, uint32_t flags, strbuf_t *buf, jsonrpc_h cb, void *userdata) {
    json_t *json = NULL;
    while (1) {
        if (!is_active)
            return NULL;
        pthread_rwlock_rdlock(&locker);
        if (fd_conn > 0)
            json = ws_jsonrpc_request_send(srv, fd_conn, method, method_len, flags, buf, cb, userdata);
        pthread_rwlock_unlock(&locker);
        if (json && -1 != (intptr_t)json)
            return json;
        if (errno == EBADF)
            _connect();
        else
            return json;
    }
}

static void term () {
    char *method;
    strbuf_t buf;
    json_t *json = NULL;
    memset(&buf, 0, sizeof buf);
    do {
        method = linenoise("> ", NULL);
        if (method && '\0' != *method && 0 != strcmp(method, "quit")) {
            int method_len = strlen(method);
            if (0 == strcmp(method, "login"))
                json = _send(method, method_len, NF_SYNC, &buf, on_login, NULL);
            else
            if (0 == strcmp(method, "getProps"))
                json = _send(method, method_len, 0, &buf, NULL, NULL);
            else
            if (0 == strcmp(method, "notify"))
                json = _send(method, method_len, NF_NOTIFY, &buf, on_notify, NULL);
            else
            if (0 == strcmp(method, "none"))
                json = _send(method, method_len, NF_SYNC, &buf, NULL, NULL);
            if (json && -1 != (intptr_t)json) {
                printf("%s\n", buf.ptr);
                json_free(json);
                json = NULL;
            }
            if (buf.ptr) {
                free(buf.ptr);
                memset(&buf, 0, sizeof buf);
            }
        }
    } while (0 != strcmp(method, "quit"));
    if (json && -1 != (intptr_t)json)
        json_free(json);
    if (buf.ptr)
        free(buf.ptr);
    ws_ev_disconnect(srv, fd_conn);
    ws_srv_done(srv);
}

int login_args [] = { JSON_STRING, JSON_STRING };
int getprops_args [] = {};
int notify_args [] = { JSON_STRING };

int main (int argc, const char *argv[]) {
    int is_client = 1;
    if (2 != argc) return 1;
    if (0 == strcmp(argv[1], "server"))
        is_client = 0;
    else
    if (0 != strcmp(argv[1], "client"))
        return 1;
    printf("[%d]\n", getpid());
    methods = jsonrpc_init_methods();
    jsonrpc_add_method(methods, 1, CONST_STR_LEN("login"), login, JSONRPC_TYPES_LEN(login_args));
    jsonrpc_add_method(methods, 2, CONST_STR_LEN("getProps"), getprops, JSONRPC_TYPES_LEN(getprops_args));
    jsonrpc_add_method(methods, 3, CONST_STR_LEN("notify"), notify, JSONRPC_TYPES_LEN(notify_args));
    if (is_client) {
        pthread_t th;
        srv = sslws_jsonrpc_srv_init(NULL, "./fullchain.pem", "./privkey.pem");
        srv->on_handle = on_handle;
        srv->tm_connect = 8;
        srv->tm_recv = 8;
        pthread_create(&th, NULL, on_start, NULL);
        sleep(1);
        if (0 == (fd_conn = net_ev_connect(srv, CONST_STR_LEN("wss://127.0.0.1:7878/api/v2")))) {
            printf("(%d) %s\n", errno, strerror(errno));
            sslws_srv_done(srv);
            free(methods);
        } else
            term();
    } else {
        setsig();
        srv = sslws_jsonrpc_srv_init("7878", "./fullchain.pem", "./privkey.pem");
        srv->on_handle = on_handle;
        srv->on_check_url = check_api_ver;
        srv->tm_ping = 60;
        printf("Ok\n");
        ws_srv_start(srv);
    }
    return 0;
}
