#!/bin/sh

valgrind --log-file=$1.log --leak-check=full --show-reachable=yes --track-origins=yes $1 $2 $3
#valgrind --leak-check=full --show-reachable=yes --track-origins=yes $1 $2 $3
